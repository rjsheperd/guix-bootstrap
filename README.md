Guix Bootstrap
============

This repository houses container images providing the [GNU Guix](https://guix.gnu.org/) package management system on [foreign distros](https://guix.gnu.org/manual/en/html_node/Installation.html#Installation). Currently, there are images for [Alpine Linux](https://alpinelinux.org/).

Tags
----

* [`1.1.0-alpine-3.11`](https://gitlab.com/singularsyntax-docker-hub/guix-bootstrap)

Usage
-----

### Dockerfile ###

[`Dockerfile`](https://docs.docker.com/engine/reference/builder/) workflows are straightforward:

    FROM singularsyntax/guix-bootstrap:1.1.0-alpine-3.11
    RUN guix pull; guix package --install <some-package>
    # ...

### Executable ###

Images can also be used as executables, e.g. for [Docker-in-Docker](https://hub.docker.com/_/docker) workflows. The entrypoint invokes `guix-daemon` so the container should be started with the `--detach` flag:

    container_id=`docker run --detach singularsyntax/guix-bootstrap:1.1.0-alpine-3.11`
    docker exec $container_id guix package --install <some-package>

To get a shell inside a running container started with `--detach`:

    docker exec -it $container_id /bin/sh

### Guix Daemon Options ###

By default, containers run *unprivileged* by passing the `--disable-chroot` option to `guix-daemon`. This is necessary for use in `Dockerfile` workflows since there is no option in `docker build` to issue `RUN` instructions in privileged mode, resulting in less deterministic build processes as [noted](https://guix.gnu.org/manual/en/html_node/Build-Environment-Setup.html) in Guix documentation. This is less of a concern for Docker use-cases since containers themselves provide a measure of isolation, and many such container instances will have an ephemeral lifespan, scoped to the duration of some automatic build process.

However, should it be necessary to run in privileged mode or pass other custom flags to `guix-daemon`, the defaults `--build-users-group=guixbuild --disable-chroot` can be overridden by setting the `GUIX_DAEMON_OPTS` container environment variable:

    docker run --detach --env GUIX_DAEMON_OPTS=--build-users-group=guixbuild --privileged singularsyntax/guix-bootstrap:1.1.0-alpine-3.11

### Shell Initialization ###

#### Non-Interactive/Non-Login Shells ####

For many common automated workflows, a shell that is neither interactive nor a login shell is invoked to execute some script text. Typically,
such a workflow will invoke `/bin/sh -c` with a string argument, or `/bin/sh -s` and pass an entire script into the shell via `STDIN`. It is
important to note that **both of these situations skip regular shell initialization mechanisms** like sourcing `/etc/profile` (for login
shells) or executing the contents of a script referred to by the `ENV` environment variable (for interactive shells). Therefore, it may be
necessary to manually adjust the shell path or environment for such scripts.

In particular, [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) does something like the following when [executing job scripts](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image) defined in `.gitlab-ci.yml`:

    cat gitlab-ci-script | docker run --interactive --entrypoint /bin/sh <image>

Note that the name of the `--interactive` flag is a bit misleading and has nothing to do with whether the shell is interactive or not; rather,
it just ensures that the process being run in the container has a `STDIN` stream.

In order for packages installed with Guix to be available on the path, it is necessary to manually source the Guix profile in the job script, e.g.:

    build:
      stage: build
      image: singularsyntax/guix-bootstrap:1.1.0-alpine-3.11
      script:
        - |
          set -o errexit
          source "$HOME/.guix-profile/etc/profile"
          # ...

Support
-------

* [Guix Help Page](https://guix.gnu.org/help/)
* [Image Issues](https://gitlab.com/singularsyntax-docker-hub/guix-bootstrap/-/issues)
