# ARG DOCKER_SUPER_IMAGE
# FROM $DOCKER_SUPER_IMAGE
# 
# ARG DOCKER_COMMIT_AUTHOR
# LABEL maintainer=$DOCKER_COMMIT_AUTHOR
FROM alpine:latest

RUN                                                                                                                     \
  cd /tmp ;                                                                                                             \
  apk add --no-cache gnupg ;                                                                                            \
  wget -q https://ftp.gnu.org/gnu/guix/guix-binary-1.4.0.x86_64-linux.tar.xz ;                                          \
  wget -q https://ftp.gnu.org/gnu/guix/guix-binary-1.4.0.x86_64-linux.tar.xz.sig ;                                      \
  wget 'https://sv.gnu.org/people/viewgpg.php?user_id=15145' -qO - | gpg --import - ;                                   \
  gpg --verify guix-binary-1.4.0.x86_64-linux.tar.xz.sig ;                                                              \
  tar -xf guix-binary-1.4.0.x86_64-linux.tar.xz ;                                                                       \
  mv var/guix /var/ && mv gnu / ;                                                                                       \
  mkdir -p ~root/.config/guix ;                                                                                         \
  ln -sf /var/guix/profiles/per-user/root/current-guix ~root/.config/guix/current ;                                     \
                                                                                                                        \
  addgroup -S guixbuild ;                                                                                               \
  for i in `seq -w 1 10`; do                                                                                            \
    adduser -G guixbuild                                                                                                \
            -h /var/empty -s `which nologin`                                                                            \
            -g "Guix build user $i" -S                                                                                  \
            guixbuilder$i ;                                                                                             \
  done ;                                                                                                                \
                                                                                                                        \
  /root/.config/guix/current/bin/guix archive --authorize < ~root/.config/guix/current/share/guix/ci.guix.gnu.org.pub ; \
  rm -f guix-binary-1.4.0.x86_64-linux.tar.xz.sig ;                                                                     \
  rm -f guix-binary-1.4.0.x86_64-linux.tar.xz ;

ADD guix-invoke.sh /etc/profile.d
RUN chmod 0755 /etc/profile.d/guix-invoke.sh
ENV ENV /etc/profile.d/guix-invoke.sh
ENV GUIX_DAEMON_OPTS "--build-users-group=guixbuild --disable-chroot"
SHELL ["/etc/profile.d/guix-invoke.sh"]

RUN guix pull

# ENTRYPOINT guix-daemon $GUIX_DAEMON_OPTS
