#!/bin/sh

guix()
{
  if ! pgrep guix-daemon > /dev/null; then
    guix-daemon $GUIX_DAEMON_OPTS > /var/log/guix-daemon.log 2>&1 &
    until [ -e /var/guix/daemon-socket/socket ]; do sleep 1; done
  fi

  command guix $@
}

GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"

if [ -e "$HOME/.guix-profile" ]; then
  GUIX_PROFILE="$HOME/.guix-profile"
  . "$GUIX_PROFILE/etc/profile"
fi

if [ $# -gt 0 ]; then
  /bin/sh -c "source /etc/profile.d/guix-invoke.sh; $@"
fi
